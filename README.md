# Ubuntu server 20.04 with prometheus node_exporter, grafana, prometheus
This solution using automation tool Vagrant with Virtualbox on Win10 + WSL2 and Ansible for server configuring.
Prometheus node_exporter installing on Ubuntu-server. Grafana and Prometheus installing on Ubuntu-server in docker containers using docker-compose.
Grafana has preconfigured datasource Prometheus and Dashboard 1860 but you can change it in grafana_dashboards variable

### Vagrant installing

You can install Vagrant on Win10 + WSL2 following by instructions on link : https://blog.thenets.org/how-to-run-vagrant-on-wsl-2/
But you have to use VPN for installing Vagrant plugin: virtualbox_WSL2 if you are in Russia.

### Ansible installing in WSL

Log in WSL Cli and perform commands:
```bash
sudo apt update && sudo apt upgrade
sudo apt install ansible
```

### Solution deploy

Go to directory with Vagrantfile in shell, and perform:
```bash
vagrant up
```
Take a few minutes (in WSL it is about 30 minutes)
and Open in browser http://localhost:3000 
You will see grafana login page. Login / password : admin / admin

Enjoy! :)